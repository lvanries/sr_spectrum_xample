from cpymad.madx import Madx

import xtrack as xt
import xpart as xp
import xobjects as xo

import numpy as np
import matplotlib.pyplot as plt
from pint import UnitRegistry

#Checks based on publication by H. Burkhardt https://cds.cern.ch/record/1038899/files/open-2007-018.pdf
#When refering to equations, this paper is meant

#Use unit registry to make sure dimensions are correct
ureg = UnitRegistry()

#Units needed for critical energy
hbar = ureg.hbar
emass = ureg.electron_mass
echarge = ureg.elementary_charge 


#Strength of magnetic field of dipole
bfield = ureg.Quantity(0.01, 'tesla')
Bfield=bfield.magnitude

#Self consistent MADX Sequence made up of one 1m dipole - energy set to 10 GeV
with Madx() as mad:

    mad.input(f'''
    dip: sbend,l:= 1,angle = 0;
    ring: sequence, l = 1;
    dip, at = 0.5;
    endsequence;
    Bfield = {bfield.magnitude};
    beam, sequence=ring,particle="electron",radiate=false,energy:= 10;
    BR = beam%ring->brho;
    dip, angle = 1 * Bfield/BR, TILT = PI/2;
    ''')
    #Tilted dipole to make sure it radiates correctly even when tilted
    mad.use('ring')

    # Makethin
    mad.input(f'''
    select, flag=MAKETHIN, SLICE=4, thick=false;
    MAKETHIN, SEQUENCE=ring, MAKEDIPEDGE=false;
    use, sequence=RING;
    ''')
    mad.use('ring')

    # Build xtrack line
    print('Build xtrack line...')
    line = xt.Line.from_madx_sequence(mad.sequence['RING'])
    line.particle_ref = xp.Particles(
            mass0=xp.ELECTRON_MASS_EV,
            q0=1,
            gamma0=mad.sequence.ring.beam.gamma)
            
    gamma=mad.sequence.ring.beam.gamma
    print(f'Gamma: {mad.sequence.ring.beam.gamma}')
# Build line
line.build_tracker()

line.configure_radiation(model='quantum')

#Three plots
fig, axs = plt.subplots(1,3)

#i represents deltap/p particle energy will be E0(1 + i)
#In this case we are sampling at 10, 100 and 200 GeV
for i in [0, 9, 19]:
    record = 0
    
    #Analytically work out critical energy based on Eq (4)
    EC = (3*hbar*((gamma*(1+i))**2)*echarge*bfield)/(2*emass)
    
    #Record the photons
    record = line.start_internal_logging_for_elements_of_type(xt.Multipole, capacity=int(1E7))
    particles = xp.build_particles(line=line, x=[0]*int(1E7), delta=[i]*int(1E7))
    line.track(particles, num_turns=1)
    line.stop_internal_logging_for_elements_of_type(xt.Multipole)
    hist, bin_edges = np.histogram(record.photon_energy[:record._index.num_recorded], bins=500)
    
    #Loglog plot of photon spectrum - expect to fall with energy
    axs[0].loglog((bin_edges[1:]+bin_edges[:-1])/2, hist/np.diff(bin_edges),label = str(np.round(xp.ELECTRON_MASS_EV*gamma*(1+i)/1E9))+"GeV")
    
    #Loglog plot of power spectrum - expect increase then decrease with energy
    axs[1].loglog((bin_edges[1:]+bin_edges[:-1])/2, hist*(0.5*(bin_edges[1:]+bin_edges[:-1]))/np.diff(bin_edges),label = str(np.round(xp.ELECTRON_MASS_EV*gamma*(1+i)/1E9))+"GeV")
    
    #Power spectrum normalised with critical energy and a line to show critical energy, like Figure 2 in publication
    axs[2].semilogy((bin_edges[1:]+bin_edges[:-1])/2/EC.to('eV').magnitude, hist*(0.5*(bin_edges[1:]+bin_edges[:-1]))/np.diff(bin_edges),label = str(np.round(xp.ELECTRON_MASS_EV*gamma*(1+i)/1E9))+"GeV")
#    axs[1].axvline(EC.to('eV').magnitude)
    axs[2].axvline(1,linestyle = ':')
    
    #Sort photons and then do a cumulative summ to identify the energy below which halft the photon energy is radiated
    #This is the definition of the critical energy
    sorted_photons = np.sort(record.photon_energy[:record._index.num_recorded])
    Total_Energy = np.sum(sorted_photons)
    Cumulsum = 0
    for photon in sorted_photons:
        Cumulsum += photon
        if Cumulsum > Total_Energy/2:
            EC_sim = photon
            break
    
    #Work out the mean and standard deviation of the photon energy
    mu = np.mean(record.photon_energy[:record._index.num_recorded]/EC.to('eV').magnitude)
    s = np.std(record.photon_energy[:record._index.num_recorded])/(EC.to('eV').magnitude)
    
    # print(f'At Electron energy of {xp.ELECTRON_MASS_EV*gamma*(1+i)} eV')
    # print(f'Total number of photons {len(sorted_photons)}')
    # print(f'1/sqrt(N) = {1/np.sqrt(len(sorted_photons))}')
    
    # print(f'Critical Energy: {EC_sim} eV')
    # print(f'Relative error of'+str(EC_sim/EC.to('eV').magnitude - 1))
    
    # print(f'Mean Energy: {mu} eV')
    # print(f'Relative error of {8/(15*np.sqrt(3))/mu - 1}')
    
    # print(f'Sigma: {s} eV')   
    # print(f'Relative error of {np.sqrt(211/675)/s - 1}')
    
    #Use 10 sigma rtol in assert testing, sigma worked out as 1/sqrt(N)
    
    #Check if critical energy matches analytical value
    assert np.isclose(EC_sim, EC.to('eV').magnitude , atol=0, rtol=10/np.sqrt(len(sorted_photons)))
    #Check if equation (19) on mean is satisfied
    assert np.isclose(mu, 8/(15*np.sqrt(3)), atol=0, rtol=10/np.sqrt(len(sorted_photons)))
    #Check if equation (20) on standard deviation is satisfied
    assert np.isclose(s, np.sqrt(211/675), atol=0, rtol=10/np.sqrt(len(sorted_photons)))

    
axs[0].set_xlabel('Photon energy [eV]')
axs[0].set_ylabel('dN/dE [1/eV]')    
axs[1].set_xlabel('Photon energy [eV]')
axs[1].set_ylabel('E dN/dE')    
axs[2].set_xlabel('Photon energy / Critical Energy')
axs[2].set_ylabel('E dN/dE')

axs[0].legend()
axs[1].legend()
axs[2].legend()

plt.show()
